module Nodeinfo2
  module Rails
    class Engine < ::Rails::Engine
      isolate_namespace Nodeinfo2::Rails

      config.to_prepare do
        Dir.glob(::Rails.root + 'app/**/*_patch*.rb').each do |c|
          require_dependency(c)
        end
      end
    end
  end
end
