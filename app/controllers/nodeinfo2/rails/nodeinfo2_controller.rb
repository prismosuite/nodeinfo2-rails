module Nodeinfo2
  module Rails
    class Nodeinfo2Controller < ActionController::Base
      def show
        render json: Nodeinfo2Serializer.new.to_json
      end
    end
  end
end
