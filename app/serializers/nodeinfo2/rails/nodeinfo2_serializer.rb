module Nodeinfo2
  module Rails
    class Nodeinfo2Serializer
      def as_json
        {
          version: version,
          server: {
            baseUrl: server_base_url,
            name: server_name,
            software: server_software,
            version: server_version
          },
          protocols: protocols,
          openRegistrations: open_registrations,
          usage: {
            users: {
              total: usage_users_total,
              activeHalfyear: usage_users_active_halfyear,
              activeMonth: usage_users_active_month
            },
            localPosts: usage_local_posts,
            localComments: usage_local_comments
          },
          services: services
        }
      end

      def to_json
        as_json.to_json
      end

      private

      def version
        '1.0'
      end

      def protocols
        ['activitypub']
      end

      def open_registrations
        raise NotImplementedError
      end

      def services
        {
          inbound: [],
          outbound: []
        }
      end

      def server_base_url
        raise NotImplementedError
      end

      def server_name
        raise NotImplementedError
      end

      def server_software
        raise NotImplementedError
      end

      def server_version
        raise NotImplementedError
      end

      def usage_users_total
        raise NotImplementedError
      end

      def usage_users_active_halfyear
        raise NotImplementedError
      end

      def usage_users_active_month
        raise NotImplementedError
      end

      def usage_local_posts
        raise NotImplementedError
      end

      def usage_local_comments
        raise NotImplementedError
      end
    end
  end
end
